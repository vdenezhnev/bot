import mysql.connector
from mysql.connector import Error

def create_connection():
    connection = None
    try:
        connection = mysql.connector.connect(
            host="mysql.kaymakmedia.myjino.ru",
            user="046590004_bot",
            passwd="SdfW89sdfawD",
            database="kaymakmedia_store-bot",
            port=3306
        )
        print("Connection to MySQL DB successful!")
    except Error as e:
        print(f"The error '{e}' occurred")

    return connection
    
def execute_query(query):
    connection = create_connection()
    cursor = connection.cursor()
    try:
        cursor.execute(query)
        connection.commit()
        print("Query executed successfully!")
    except Error as e:
        print(f"The error '{e}' occurred")
    finally:
        connection.close()


'''
query = f"INSERT INTO `business` (`ID_BUSINESS`, `NUMBER_BUSINESS`, `FIO_BUSINESS`, `TOB_BUSINESS`, `ADRESS_BUSINESS`, `GOLD_BUSINESS`) VALUES (123123, 223123123, 'Egor Solovey', 'type asd', 'shirokiy 12', 100)"

def execute_query(connection, query):
    cursor = connection.cursor()
    try:
        cursor.execute(query)
        connection.commit()
        print("Query executed successfully")
    except Error as e:
        print(f"The error '{e}' occurred")

execute_query(connection, query)
'''