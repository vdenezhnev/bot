import asyncio
import time

from aiogram.utils import executor
from create_bot import dp
from keyboards.admin import main_kb

from handlers import client, admin, other, tovar
from keyboards.cron_uppdate import cron
from names import admin_ID

client.register_handlers_client(dp) #поключение команд
other.register_handlers_other(dp) #поключение команд
admin.register_handlers_admin(dp)
tovar.register_handlers_tovar(dp)


loop = asyncio.new_event_loop()
asyncio.set_event_loop(loop)
loop.run_until_complete(cron())

'''
loop_query_db = asyncio.new_event_loop()
asyncio.set_event_loop(loop_query_db)
loop_query_db.call_soon(query_db, loop_query_db)

try:
    loop_query_db.run_forever()
finally:
    loop_query_db.close()
'''

executor.start_polling(dp, skip_updates=True)  # команда, которая говорит боту не отвечать на сообщения, отправленные в неактивное время