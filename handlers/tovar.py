from aiogram.dispatcher import FSMContext
from aiogram.dispatcher.filters import Text
from aiogram.dispatcher.filters.state import State, StatesGroup
from aiogram import Dispatcher
from aiogram import types
import Date_base

from create_bot import dp
from keyboards.admin import main_kb, nazad
from keyboards.client import kb_klient_2, kb_month, value, kb_vibor, value_1
from aiogram.types import ReplyKeyboardRemove, ReplyKeyboardMarkup, KeyboardButton


class FSMRegTovar(StatesGroup):
    tovar = State()
    ID_User = State()
    Kategory = State()
    Ser_Kategori = State()
    NameOfBuy = State()
    CostAdmin = State()
    CostUser = State()
    FirstPart = State()
    TimeOfParts = State()
    Percent = State()
    Kategori = State()
    Photo_1 = State()
    Vibor_Photo_2 = State()
    Photo_2 = State()
    Vibor_Photo_3 = State()
    Photo_3 = State()
    Vibor_Photo_4 = State()
    Photo_4 = State()
    Vibor_Photo_5 = State()
    Photo_5 = State()
    Vibor_Photo_6 = State()
    Photo_6 = State()
    Vibor_Photo_7 = State()
    Photo_7 = State()
    Vibor_Photo_8 = State()
    Photo_8 = State()
    Vibor_Photo_9 = State()
    Photo_9 = State()
    Vibor_Photo_10 = State()
    Photo_10 = State()
    Vibor_Video_1 = State()
    Video_1 = State()
    Vibor_Video_2 = State()
    Video_2 = State()
    Vibor_Video_3 = State()
    Video_3 = State()


# Начало диалога загрузки нового пункта меню
@dp.callback_query_handler(text='add_tovar')
async def add_tovar_call(callback : types.CallbackQuery):
    await FSMRegTovar.ID_User.set()
    await callback.message.answer('Добавить товар', reply_markup=kb_klient_2)

# @dp.message_handler(state=FSMRegistartion.ID)
async def load_ID(message: types.Message, state: FSMContext):
    async with state.proxy() as data:
        data['ID_User'] = message.from_user.id
        data['Photo_1'] = ' '
        data['Photo_2'] = ' '
        data['Photo_3'] = ' '
        data['Photo_4'] = ' '
        data['Photo_5'] = ' '
        data['Photo_6'] = ' '
        data['Photo_7'] = ' '
        data['Photo_8'] = ' '
        data['Photo_9'] = ' '
        data['Photo_10'] = ' '
        data['Video_1'] = ' '
        data['Video_2'] = ' '
        data['Video_3'] = ' '
    await FSMRegTovar.next()
    await message.answer('Для отмены нажмите на кнопку', reply_markup=nazad)
    my_clients_test = ""
    my_clients = f""" SELECT PRODUCT_CATIGORIES FROM product_categories WHERE PARENT = 0"""
    connection = Date_base.create_connection()
    with connection.cursor() as cursor:
        cursor.execute(my_clients)
        for my_clients_list in cursor.fetchall():
            my_clients_test = my_clients_test + (f" {my_clients_list[0]}")
    a = my_clients_test.split(' ')
    a.pop(0)
    print(a)
    keyboard = ReplyKeyboardMarkup(resize_keyboard=True, one_time_keyboard=True).row(
        *[KeyboardButton(text=x) for x in a])
    await message.answer('Выберите категорию товара', reply_markup=keyboard)


async def load_Kategory(message: types.Message, state: FSMContext):
    async with state.proxy() as data:
        data['Kategory'] = message.text
    my_clients_test = ""
    my_prod = f""" SELECT * FROM product_categories WHERE PRODUCT_CATIGORIES = '{str(data['Kategory'])}'"""
    connection = Date_base.create_connection()
    with connection.cursor() as cursor:
        cursor.execute(my_prod)
        for my_prod_list in cursor.fetchall():
            my_clients = f""" SELECT PRODUCT_CATIGORIES FROM product_categories WHERE PARENT = {my_prod_list[0]}"""
            with connection.cursor() as cursor:
                cursor.execute(my_clients)
                for my_clients_list in cursor.fetchall():
                    my_clients_test = my_clients_test + (f" {my_clients_list[0]}")
    a = my_clients_test.split(' ')
    a.pop(0)
    print(a)
    keyboard = ReplyKeyboardMarkup(resize_keyboard=True, one_time_keyboard=True).row(*[KeyboardButton(text=x) for x in a])
    await FSMRegTovar.next()
    await message.answer('Для отмены нажмите на кнопку', reply_markup=nazad)
    await message.answer('Выберите подкатегорию товара', reply_markup=keyboard)

@dp.message_handler(state=FSMRegTovar.Ser_Kategori)
async def load_Ser_Kategory(message: types.Message, state: FSMContext):
    async with state.proxy() as data:
        data['Ser_Kategori'] = message.text
    await FSMRegTovar.next()
    await message.answer('Для отмены нажмите на кнопку', reply_markup=nazad)
    await message.answer('Введите название товара')

async def load_NameOfBuy(message: types.Message, state: FSMContext):
    async with state.proxy() as data:
        data['NameOfBuy'] = message.text
    await FSMRegTovar.next()
    await message.answer('Для отмены нажмите на кнопку', reply_markup=nazad)
    await message.answer('Введите свою цену')


async def load_CostAdmin(message: types.Message, state: FSMContext):
    if(message.text.isdigit() == True):
        async with state.proxy() as data:
            data['CostAdmin'] = message.text
        await FSMRegTovar.next()
        await message.answer('Для отмены нажмите на кнопку', reply_markup=nazad)
        await message.answer('Введите цену для покупателей')
    else:
        await message.answer('Неверно \nВведите свою цену')
        await FSMRegTovar.CostAdmin.set()


async def load_CostUser(message: types.Message, state: FSMContext):
    if (message.text.isdigit() == True):
        async with state.proxy() as data:
            data['CostUser'] = message.text
        await FSMRegTovar.next()
        await message.answer('Для отмены нажмите на кнопку', reply_markup=nazad)
        await message.answer('Введите первоначальный взнос')
    else:
        await message.answer('Неверно \nВведите цену для покупателей')
        await FSMRegTovar.CostUser.set()


async def load_FirstPart(message: types.Message, state: FSMContext):
    if (message.text.isdigit() == True):
        async with state.proxy() as data:
            data['FirstPart'] = message.text
        await FSMRegTovar.next()
        await message.answer('Для отмены нажмите на кнопку', reply_markup=nazad)
        await message.answer('Выберите срок рассрочки', reply_markup=kb_month)
    else:
        await message.answer('Неверно \nВведите первоначальный взнос')
        await FSMRegTovar.FirstPart.set()


async def load_TimeOfParts(message: types.Message, state: FSMContext):
    async with state.proxy() as data:
        data['TimeOfParts'] = message.text
        a = (int(data['CostUser'])-int(data['FirstPart']))/int(data['TimeOfParts'])
        data['Percent'] = a
    await message.answer(f"Ежемесячная оплата:{a}\nВыберите загружаемые файлы", reply_markup=kb_vibor)
    await message.answer('Для отмены нажмите на кнопку', reply_markup=nazad)
    await FSMRegTovar.Kategori.set()

async def load_Kategori(message: types.Message, state: FSMContext):
    async with state.proxy() as data:
        data['Kategori'] = message.text
    if(message.text=='Перейти к загрузке видео'):
        await FSMRegTovar.Video_1.set()
        await message.answer('Для отмены нажмите на кнопку', reply_markup=nazad)
        await message.answer('Загрузите видео товара',reply_markup=ReplyKeyboardRemove())
    elif(message.text=='Перейти к загрузке фото'):
        await FSMRegTovar.Photo_1.set()
        await message.answer('Для отмены нажмите на кнопку', reply_markup=nazad)
        await message.answer('Загрузите фото',reply_markup=ReplyKeyboardRemove())
    else:
        await message.answer("Неверно \nВыберите загружаемые файлы", reply_markup=kb_vibor)
        await FSMRegTovar.Kategori.set()

@dp.message_handler(content_types=['photo'], state=FSMRegTovar.Photo_1)
async def load_Photo_1(message: types.Message, state: FSMContext):
        async with state.proxy() as data:
            data['Photo_1'] = ' '
            data['Photo_1'] = message.photo[0].file_id
        await FSMRegTovar.next()
        await message.answer('Для отмены нажмите на кнопку', reply_markup=nazad)
        await message.answer('Загрузить еще фото?', reply_markup=value)


async def load_Vibor_Photo_2(message: types.Message, state: FSMContext):
    async with state.proxy() as data:
        data['Vibor_Photo_2'] = message.text
    if(str(message.text).lower() == 'да'):
        await message.answer('Для отмены нажмите на кнопку', reply_markup=nazad)
        await message.answer('Загрузите еще фото', reply_markup=ReplyKeyboardRemove())
        await FSMRegTovar.Photo_2.set()
    elif(str(message.text).lower() == 'нет'):
        await message.answer('Загрузите видео или добавить товар', reply_markup=value_1)
        await FSMRegTovar.Vibor_Video_1.set()

@dp.message_handler(content_types=['photo'], state=FSMRegTovar.Photo_2)
async def load_Photo_2(message: types.Message, state: FSMContext):
        async with state.proxy() as data:
            data['Photo_2'] = message.photo[0].file_id
        await FSMRegTovar.next()
        await message.answer('Для отмены нажмите на кнопку', reply_markup=nazad)
        await message.answer('Загрузить еще фото?', reply_markup=value)

async def load_Vibor_Photo_3(message: types.Message, state: FSMContext):
    async with state.proxy() as data:
        data['Vibor_Photo_3'] = message.text
    if(str(message.text).lower() == 'да'):
        await message.answer('Для отмены нажмите на кнопку', reply_markup=nazad)
        await message.answer('Загрузите еще фото', reply_markup=ReplyKeyboardRemove())
        await FSMRegTovar.Photo_3.set()
    elif(str(message.text).lower() == 'нет'):
        await message.answer('Загрузите видео или добавить товар', reply_markup=value_1)
        await FSMRegTovar.Vibor_Video_1.set()


@dp.message_handler(content_types=['photo'], state=FSMRegTovar.Photo_3)
async def load_Photo_3(message: types.Message, state: FSMContext):
    async with state.proxy() as data:
        data['Photo_3'] = message.photo[0].file_id
    await FSMRegTovar.next()
    await message.answer('Для отмены нажмите на кнопку', reply_markup=nazad)
    await message.answer('Загрузить еще фото?', reply_markup=value)


async def load_Vibor_Photo_4(message: types.Message, state: FSMContext):
    async with state.proxy() as data:
        data['Vibor_Photo_4'] = message.text
    if (str(message.text).lower() == 'да'):
        await message.answer('Для отмены нажмите на кнопку', reply_markup=nazad)
        await message.answer('Загрузите еще фото', reply_markup=ReplyKeyboardRemove())
        await FSMRegTovar.Photo_4.set()
    elif (str(message.text).lower() == 'нет'):
        await message.answer('Загрузите видео или добавить товар', reply_markup=value_1)
        await FSMRegTovar.Vibor_Video_1.set()


@dp.message_handler(content_types=['photo'], state=FSMRegTovar.Photo_4)
async def load_Photo_4(message: types.Message, state: FSMContext):
    async with state.proxy() as data:
        data['Photo_4'] = message.photo[0].file_id
    await FSMRegTovar.next()
    await message.answer('Для отмены нажмите на кнопку', reply_markup=nazad)
    await message.answer('Загрузить еще фото?', reply_markup=value)


async def load_Vibor_Photo_5(message: types.Message, state: FSMContext):
    async with state.proxy() as data:
        data['Vibor_Photo_5'] = message.text
    if (str(message.text).lower() == 'да'):
        await message.answer('Для отмены нажмите на кнопку', reply_markup=nazad)
        await message.answer('Загрузите еще фото', reply_markup=ReplyKeyboardRemove())
        await FSMRegTovar.Photo_5.set()
    elif (str(message.text).lower() == 'нет'):
        await message.answer('Загрузите видео или добавить товар', reply_markup=value_1)
        await FSMRegTovar.Vibor_Video_1.set()


@dp.message_handler(content_types=['photo'], state=FSMRegTovar.Photo_5)
async def load_Photo_5(message: types.Message, state: FSMContext):
    async with state.proxy() as data:
        data['Photo_5'] = message.photo[0].file_id
    await FSMRegTovar.next()
    await message.answer('Для отмены нажмите на кнопку', reply_markup=nazad)
    await message.answer('Загрузить еще фото?', reply_markup=value)


async def load_Vibor_Photo_6(message: types.Message, state: FSMContext):
    async with state.proxy() as data:
        data['Vibor_Photo_6'] = message.text
    if (str(message.text).lower() == 'да'):
        await message.answer('Для отмены нажмите на кнопку', reply_markup=nazad)
        await message.answer('Загрузите еще фото', reply_markup=ReplyKeyboardRemove())
        await FSMRegTovar.Photo_6.set()
    elif (str(message.text).lower() == 'нет'):
        await message.answer('Загрузите видео или добавить товар', reply_markup=value_1)
        await FSMRegTovar.Vibor_Video_1.set()


@dp.message_handler(content_types=['photo'], state=FSMRegTovar.Photo_6)
async def load_Photo_6(message: types.Message, state: FSMContext):
    async with state.proxy() as data:
        data['Photo_6'] = message.photo[0].file_id
    await FSMRegTovar.next()
    await message.answer('Для отмены нажмите на кнопку', reply_markup=nazad)
    await message.answer('Загрузить еще фото?', reply_markup=value)


async def load_Vibor_Photo_7(message: types.Message, state: FSMContext):
    async with state.proxy() as data:
        data['Vibor_Photo_7'] = message.text
    if (str(message.text).lower() == 'да'):
        await message.answer('Для отмены нажмите на кнопку', reply_markup=nazad)
        await message.answer('Загрузите еще фото', reply_markup=ReplyKeyboardRemove())
        await FSMRegTovar.Photo_7.set()
    elif (str(message.text).lower() == 'нет'):
        await message.answer('Загрузите видео или добавить товар', reply_markup=value_1)
        await FSMRegTovar.Vibor_Video_1.set()


@dp.message_handler(content_types=['photo'], state=FSMRegTovar.Photo_7)
async def load_Photo_7(message: types.Message, state: FSMContext):
    async with state.proxy() as data:
        data['Photo_7'] = message.photo[0].file_id
    await FSMRegTovar.next()
    await message.answer('Для отмены нажмите на кнопку', reply_markup=nazad)
    await message.answer('Загрузить еще фото?', reply_markup=value)


async def load_Vibor_Photo_8(message: types.Message, state: FSMContext):
    async with state.proxy() as data:
        data['Vibor_Photo_8'] = message.text
    if (str(message.text).lower() == 'да'):
        await message.answer('Для отмены нажмите на кнопку', reply_markup=nazad)
        await message.answer('Загрузите еще фото', reply_markup=ReplyKeyboardRemove())
        await FSMRegTovar.Photo_8.set()
    elif (str(message.text).lower() == 'нет'):
        await message.answer('Загрузите видео или добавить товар', reply_markup=value_1)
        await FSMRegTovar.Vibor_Video_1.set()


@dp.message_handler(content_types=['photo'], state=FSMRegTovar.Photo_8)
async def load_Photo_8(message: types.Message, state: FSMContext):
    async with state.proxy() as data:
        data['Photo_8'] = message.photo[0].file_id
    await FSMRegTovar.next()
    await message.answer('Для отмены нажмите на кнопку', reply_markup=nazad)
    await message.answer('Загрузить еще фото?', reply_markup=value)


async def load_Vibor_Photo_9(message: types.Message, state: FSMContext):
    async with state.proxy() as data:
        data['Vibor_Photo_9'] = message.text
    if (str(message.text).lower() == 'да'):
        await message.answer('Для отмены нажмите на кнопку', reply_markup=nazad)
        await message.answer('Загрузите еще фото', reply_markup=ReplyKeyboardRemove())
        await FSMRegTovar.Photo_9.set()
    elif (str(message.text).lower() == 'нет'):
        await message.answer('Загрузите видео или добавить товар', reply_markup=value_1)
        await FSMRegTovar.Vibor_Video_1.set()

@dp.message_handler(content_types=['photo'], state=FSMRegTovar.Photo_9)
async def load_Photo_9(message: types.Message, state: FSMContext):
    async with state.proxy() as data:
        data['Photo_9'] = message.photo[0].file_id
    await FSMRegTovar.next()
    await message.answer('Для отмены нажмите на кнопку', reply_markup=nazad)
    await message.answer('Загрузить еще фото?', reply_markup=value)


async def load_Vibor_Photo_10(message: types.Message, state: FSMContext):
    async with state.proxy() as data:
        data['Vibor_Photo_10'] = message.text
    if (str(message.text).lower() == 'да'):
        await message.answer('Для отмены нажмите на кнопку', reply_markup=nazad)
        await message.answer('Загрузите еще фото', reply_markup=ReplyKeyboardRemove())
        await FSMRegTovar.Photo_8.set()
    elif (str(message.text).lower() == 'нет'):
        await message.answer('Загрузите видео или добавить товар', reply_markup=value_1)
        await FSMRegTovar.Vibor_Video_1.set()

@dp.message_handler(content_types=['photo'], state=FSMRegTovar.Photo_10)
async def load_Photo_10(message: types.Message, state: FSMContext):
    async with state.proxy() as data:
        data['Photo_10'] = message.photo[0].file_id
    await FSMRegTovar.next()
    await message.answer('Для отмены нажмите на кнопку', reply_markup=nazad)
    await message.answer('Загрузите видео или добавить товар', reply_markup=value_1)




async def load_Vibor_Video_1(message: types.Message, state: FSMContext):
    async with state.proxy() as data:
        data['Vibor_Video_1'] = message.text
    if(str(message.text).lower() == 'загрузить видео'):
        await message.answer('Загрузите видео')
        await FSMRegTovar.Video_1.set()
    elif(str(message.text).lower() == 'добавить товар'):
        #async with state.proxy() as data:
            #await message.answer(str(data))

        async with state.proxy() as data:
            Date_base.execute_query(f"INSERT INTO product (	ID_BUSINESSMAN, CATEGORIES_PRODUCT, NAME_PRODUCT, PRICE_PRODUCT, NEWPRICE_PRODUCT, PAYMENT_PRODUCT, TERM_PRODUCT, PAYMENT_PERCENT_PRODUCT, PH1_PRODUCT, PH2_PRODUCT, PH3_PRODUCT, PH4_PRODUCT, PH5_PRODUCT, PH6_PRODUCT, PH7_PRODUCT, PH8_PRODUCT, PH9_PRODUCT, PH10_PRODUCT, VIDEO_PRODUCT_1, VIDEO_PRODUCT_2, VIDEO_PRODUCT_3) VALUES ('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')" % (data['ID_User'], data['Ser_Kategori'], data['NameOfBuy'], data['CostAdmin'],data['CostUser'], data['FirstPart'], data['TimeOfParts'], data['Percent'], data['Photo_1'], data['Photo_2'], data['Photo_3'], data['Photo_4'], data['Photo_5'], data['Photo_6'], data['Photo_7'], data['Photo_8'], data['Photo_9'], data['Photo_10'], data['Video_1'], data['Video_2'], data['Video_3']))
        await state.finish()
        await message.answer('выберите действие', reply_markup=main_kb)


@dp.message_handler(content_types=['video'], state=FSMRegTovar.Video_1)
async def load_Video_1(message: types.Message, state: FSMContext):
    async with state.proxy() as data:
        data['Video_1'] = message.video.file_id
    await message.answer('Для отмены нажмите на кнопку', reply_markup=nazad)
    await message.answer('Загрузите видео или добавить товар', reply_markup=value_1)
    await FSMRegTovar.Vibor_Video_2.set()


async def load_Vibor_Video_2(message: types.Message, state: FSMContext):
    async with state.proxy() as data:
        data['Vibor_Video_2'] = message.text
    if(str(message.text).lower() == 'загрузить видео'):
        await message.answer('Для отмены нажмите на кнопку', reply_markup=nazad)
        await message.answer('Загрузите видео')
        await FSMRegTovar.Video_2.set()
    elif(str(message.text).lower() == 'добавить товар'):
        #async with state.proxy() as data:
            #await message.answer(str(data))

        async with state.proxy() as data:
            Date_base.execute_query(f"INSERT INTO product (	ID_BUSINESSMAN, CATEGORIES_PRODUCT, NAME_PRODUCT, PRICE_PRODUCT, NEWPRICE_PRODUCT, PAYMENT_PRODUCT, TERM_PRODUCT, PAYMENT_PERCENT_PRODUCT, CONDITIONS_PRODUCT, PH1_PRODUCT, PH2_PRODUCT, PH3_PRODUCT, PH4_PRODUCT, PH5_PRODUCT, PH6_PRODUCT, PH7_PRODUCT, PH8_PRODUCT, PH9_PRODUCT, PH10_PRODUCT, VIDEO_PRODUCT_1, VIDEO_PRODUCT_2, VIDEO_PRODUCT_3) VALUES ('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')" % (data['ID_User'], data['Ser_Kategori'], data['NameOfBuy'], data['CostAdmin'],data['CostUser'], data['FirstPart'], data['TimeOfParts'], data['Percent'], data['Rules'], data['Photo_1'], data['Photo_2'], data['Photo_3'], data['Photo_4'], data['Photo_5'], data['Photo_6'], data['Photo_7'], data['Photo_8'], data['Photo_9'], data['Photo_10'], data['Video_1'], data['Video_2'], data['Video_3']))
        await state.finish()
        await message.answer('выберите действие', reply_markup=main_kb)

@dp.message_handler(content_types=['video'], state=FSMRegTovar.Video_2)
async def load_Video_2(message: types.Message, state: FSMContext):
    async with state.proxy() as data:
        data['Video_2'] = message.video.file_id
    await message.answer('Для отмены нажмите на кнопку', reply_markup=nazad)
    await message.answer('Загрузите видео или добавить товар', reply_markup=value_1)
    await FSMRegTovar.Vibor_Video_3.set()


async def load_Vibor_Video_3(message: types.Message, state: FSMContext):
    async with state.proxy() as data:
        data['Vibor_Video_3'] = message.text
    if(str(message.text).lower() == 'загрузить видео'):
        await message.answer('Для отмены нажмите на кнопку', reply_markup=nazad)
        await message.answer('Загрузите видео')
        await FSMRegTovar.Video_3.set()
    elif(str(message.text).lower() == 'добавить товар'):
        #async with state.proxy() as data:
            #await message.answer(str(data))

        async with state.proxy() as data:
            Date_base.execute_query(f"INSERT INTO product (	ID_BUSINESSMAN, CATEGORIES_PRODUCT, NAME_PRODUCT, PRICE_PRODUCT, NEWPRICE_PRODUCT, PAYMENT_PRODUCT, TERM_PRODUCT, PAYMENT_PERCENT_PRODUCT, PH1_PRODUCT, PH2_PRODUCT, PH3_PRODUCT, PH4_PRODUCT, PH5_PRODUCT, PH6_PRODUCT, PH7_PRODUCT, PH8_PRODUCT, PH9_PRODUCT, PH10_PRODUCT, VIDEO_PRODUCT_1, VIDEO_PRODUCT_2, VIDEO_PRODUCT_3) VALUES ('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')" % (data['ID_User'], data['Ser_Kategori'], data['NameOfBuy'], data['CostAdmin'],data['CostUser'], data['FirstPart'], data['TimeOfParts'], data['Percent'],  data['Photo_1'], data['Photo_2'], data['Photo_3'], data['Photo_4'], data['Photo_5'], data['Photo_6'], data['Photo_7'], data['Photo_8'], data['Photo_9'], data['Photo_10'], data['Video_1'], data['Video_2'], data['Video_3']))
        await state.finish()
        await message.answer('выберите действие', reply_markup=main_kb)

@dp.message_handler(content_types=['video'], state=FSMRegTovar.Video_3)
async def load_Video_3(message: types.Message, state: FSMContext):
    async with state.proxy() as data:
        data['Video_3'] = message.video.file_id
    #async with state.proxy() as data:
        #await message.answer(str(data))

    async with state.proxy() as data:
        Date_base.execute_query(f"INSERT INTO product (	ID_BUSINESSMAN, CATEGORIES_PRODUCT,NAME_PRODUCT, PRICE_PRODUCT, NEWPRICE_PRODUCT, PAYMENT_PRODUCT, TERM_PRODUCT, PAYMENT_PERCENT_PRODUCT, CONDITIONS_PRODUCT, PH1_PRODUCT, PH2_PRODUCT, PH3_PRODUCT, PH4_PRODUCT, PH5_PRODUCT, PH6_PRODUCT, PH7_PRODUCT, PH8_PRODUCT, PH9_PRODUCT, PH10_PRODUCT, VIDEO_PRODUCT_1, VIDEO_PRODUCT_2, VIDEO_PRODUCT_3) VALUES ('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')" % (
        data['ID_User'], data['Ser_Kategori'], data['NameOfBuy'], data['CostAdmin'], data['CostUser'], data['FirstPart'],
        data['TimeOfParts'], data['Percent'], data['Rules'], data['Photo_1'], data['Photo_2'], data['Photo_3'],
        data['Photo_4'], data['Photo_5'], data['Photo_6'], data['Photo_7'], data['Photo_8'], data['Photo_9'],
        data['Photo_10'], data['Video_1'], data['Video_2'], data['Video_3']))
    await message.answer("Товар успешно добавлен")
    await state.finish()
    await message.answer('выберите действие', reply_markup=main_kb)





@dp.callback_query_handler(state="*", text='назад')
@dp.message_handler(Text(equals='назад', ignore_case=True), state="*")
async def cancel_handler_call(callback : types.CallbackQuery, state: FSMContext):
    current_state = await state.get_state()
    if current_state is None:
        return
    await state.finish()
    await callback.message.reply("OK",reply_markup=ReplyKeyboardRemove())
    await callback.message.answer('выберите действие', reply_markup=main_kb)


def register_handlers_tovar(dp: Dispatcher):
    dp.register_message_handler(load_ID, state=FSMRegTovar.ID_User)
    dp.register_message_handler(load_Kategory, state=FSMRegTovar.Kategory)
    dp.register_message_handler(load_NameOfBuy, state=FSMRegTovar.NameOfBuy)
    dp.register_message_handler(load_CostAdmin, state=FSMRegTovar.CostAdmin)
    dp.register_message_handler(load_CostUser, state=FSMRegTovar.CostUser)
    dp.register_message_handler(load_FirstPart, state=FSMRegTovar.FirstPart)
    dp.register_message_handler(load_TimeOfParts, state=FSMRegTovar.TimeOfParts)
    dp.register_message_handler(load_Kategori, state=FSMRegTovar.Kategori)
    dp.register_message_handler(load_Vibor_Photo_2, state=FSMRegTovar.Vibor_Photo_2)
    dp.register_message_handler(load_Vibor_Photo_3, state=FSMRegTovar.Vibor_Photo_3)
    dp.register_message_handler(load_Vibor_Photo_4, state=FSMRegTovar.Vibor_Photo_4)
    dp.register_message_handler(load_Vibor_Photo_5, state=FSMRegTovar.Vibor_Photo_5)
    dp.register_message_handler(load_Vibor_Photo_6, state=FSMRegTovar.Vibor_Photo_6)
    dp.register_message_handler(load_Vibor_Photo_7, state=FSMRegTovar.Vibor_Photo_7)
    dp.register_message_handler(load_Vibor_Photo_8, state=FSMRegTovar.Vibor_Photo_8)
    dp.register_message_handler(load_Vibor_Photo_9, state=FSMRegTovar.Vibor_Photo_9)
    dp.register_message_handler(load_Vibor_Photo_10, state=FSMRegTovar.Vibor_Photo_10)
    dp.register_message_handler(load_Vibor_Video_1, state=FSMRegTovar.Vibor_Video_1)
    dp.register_message_handler(load_Vibor_Video_2, state=FSMRegTovar.Vibor_Video_2)
    dp.register_message_handler(load_Vibor_Video_3, state=FSMRegTovar.Vibor_Video_3)

