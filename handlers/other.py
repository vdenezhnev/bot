from aiogram.dispatcher import FSMContext
from aiogram.dispatcher.filters import Text
from aiogram.dispatcher.filters.state import State, StatesGroup
from aiogram import Dispatcher
from aiogram import types

from create_bot import dp, Bot
from aiogram.types import ReplyKeyboardRemove

from keyboards.admin import inl_kb_1,main_kb
from keyboards.client import kb_addMoney, kb_klient_4
from names import site_link, bot_link


async def echo(message: types.Message):  # параметр : анотация типа для

    if(message.text) == '/keyboard':
        await message.answer('Выберите команду', reply_markup=main_kb)

def register_handlers_other(dp: Dispatcher):
    dp.register_message_handler(echo)

