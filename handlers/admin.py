from aiogram.dispatcher import FSMContext
from aiogram.dispatcher.filters import Text
from aiogram.dispatcher.filters.state import State, StatesGroup
from aiogram import Dispatcher
from aiogram import types
import Date_base
import random
import string

from create_bot import dp, Bot
from keyboards.admin import main_kb
from keyboards.client import kb_klient_1, kb_klient_2, telephone
from aiogram.types import ReplyKeyboardRemove

random.seed()


class FSMRegistartion(StatesGroup):
    ID = State()
    number = State()
    Name = State()
    Surename = State()
    TypeOfBuy = State()
    Name_Buisness = State()
    Adress = State()


# Начало диалога загрузки нового пункта меню
@dp.message_handler(commands='start', state=None)
async def cm_start(message: types.Message):
    await FSMRegistartion.ID.set()
    await message.answer('Начало регистрации', reply_markup=kb_klient_2)


@dp.message_handler(state=FSMRegistartion.ID)
async def load_ID(message: types.Message, state: FSMContext):
    query_adm = f"SELECT  COUNT(ID_BUSINESS) FROM business WHERE ID_BUSINESS={message.from_user.id}"
    connection = Date_base.create_connection()
    with connection.cursor() as cursor:
        cursor.execute(query_adm)
        for my_clients_list in cursor.fetchall():
            a = my_clients_list[0]
            if (a == 0):
                async with state.proxy() as data:
                    data['ID'] = message.from_user.id
                await message.answer('Передать свой номер телефона', reply_markup=telephone)
                await FSMRegistartion.next()
            elif(a == 1):
                await state.finish()
                await message.answer('Вы уже зарегестрированы')
                await message.answer('выберите действие', reply_markup=main_kb)

# ловим ответ от пользователя

@dp.message_handler(content_types=['contact'], state=FSMRegistartion.number)
async def load_number(message: types.contact, state: FSMContext):
    async with state.proxy() as data:
        data['number'] = message.contact.phone_number
    await message.answer('Введите имя')
    await FSMRegistartion.next()


@dp.message_handler(state=FSMRegistartion.Name)
async def load_Name(message: types.Message, state: FSMContext):
    if (message.text.isalpha() == True):
        async with state.proxy() as data:
            data['Name'] = message.text
        await FSMRegistartion.next()
        await message.answer('Введите фамилию')
    else:
        await message.answer(f'Неверно \nВведите имя')
        await FSMRegistartion.Name.set()


async def load_Surename(message: types.Message, state: FSMContext):
    if (message.text.isalpha() == True):
        async with state.proxy() as data:
            data['Surename'] = str(message.text)
        await FSMRegistartion.next()
        await message.answer('Выберите магазин из предложенных', reply_markup=kb_klient_1)
    else:
        await message.answer(f'Неверно \nВведите фамилию')
        await FSMRegistartion.Surename.set()


# @dp.message_handler(state=FSMAdmin.TypeOfBuy)
async def load_TypeOfBuy(message: types.Message, state: FSMContext):
    if(message.text=="Магазин" or message.text=="Микрокредитная организация"):
        async with state.proxy() as data:
            data['TypeOfBuy'] = str(message.text)
        await FSMRegistartion.next()
        await message.answer('введите название магазина', reply_markup=ReplyKeyboardRemove())
    else:
        await message.answer(f'Неверно \nВыберите магазин из предложенных', reply_markup=kb_klient_1)
        await FSMRegistartion.TypeOfBuy.set()


async def load_Name_Buisness(message: types.Message, state: FSMContext):
    async with state.proxy() as data:
        data['Name_Buisness'] = str(message.text)
    await FSMRegistartion.next()
    await message.answer('введите адрес магазина')


# @dp.message_handler(state=FSMAdmin.Adress)
async def load_Adress(message: types.Message, state: FSMContext):
    async with state.proxy() as data:
        data['Adress'] = str(message.text)

    # async with state.proxy() as data:
    # await message.answer(str(data))

    async with state.proxy() as data:
        Date_base.execute_query(f"INSERT INTO business (ID_BUSINESS, NAME_BUSINES, SURNAME_BUSINES ,NUMBER_BUSINESS, TOB_BUSINESS, NAME_OF_BUSINESS ,ADRESS_BUSINESS, LINK_BUSINESS) VALUES ('%s','%s','%s','%s','%s','%s','%s','%s')" % (
        data['ID'], str(data['Name']), data['Surename'], data['number'], data['TypeOfBuy'], data['Name_Buisness'],
        data['Adress'], ''.join(random.sample((string.ascii_letters + string.digits), 10))))
    await message.answer("Регистрация прошла успешно")
    await state.finish()
    await message.answer('выберите действие', reply_markup=main_kb)


def register_handlers_admin(dp: Dispatcher):
    dp.register_message_handler(cm_start, commands=['start'], state=None)
    dp.register_message_handler(load_ID, state=FSMRegistartion.ID)
    dp.register_message_handler(load_number, state=FSMRegistartion.number)
    dp.register_message_handler(load_Surename, state=FSMRegistartion.Surename)
    dp.register_message_handler(load_TypeOfBuy, state=FSMRegistartion.TypeOfBuy)
    dp.register_message_handler(load_Name_Buisness, state=FSMRegistartion.Name_Buisness)
    dp.register_message_handler(load_Adress, state=FSMRegistartion.Adress)




