from datetime import datetime

from aiogram.dispatcher import FSMContext
from aiogram.dispatcher.filters import Text
from aiogram.dispatcher.filters.state import State, StatesGroup
from aiogram import Dispatcher
from aiogram import types
import Date_base

from create_bot import dp, Bot
from keyboards.admin import main_kb, nazad
from keyboards.client import kb_skip, kb_klient_2, messenger, Dalee, client_accept, value
from aiogram.types import ReplyKeyboardRemove, ReplyKeyboardMarkup, KeyboardButton


class FSMRegUser(StatesGroup):
    ID = State()
    FName = State()
    SName = State()
    LName = State()
    Number = State()
    Telegram = State()
    Number_Telegram = State()
    Whatsapp = State()
    Number_WhatsApp = State()
    Photo = State()
    PassportSeries = State()
    PassportID = State()
    INN = State()
    PassportDate = State()
    PhotoPassOne = State()
    PhotoPassTwo = State()
    AdressBorn = State()
    AdressNow = State()
    AnketPhoto = State()
    Podtverdit = State()
    Import = State()






# Начало диалога загрузки нового пункта меню
@dp.callback_query_handler(text='add_user')
async def add_user_call(callback : types.CallbackQuery):
    await FSMRegUser.ID.set()
    await callback.message.answer('Начало регистрации', reply_markup=kb_klient_2)


# @dp.message_handler(state=FSMRegistartion.ID)
async def load_ID(message: types.Message, state: FSMContext):
    async with state.proxy() as data:
        data['ID'] = message.from_user.id
    await FSMRegUser.next()
    await message.answer('Для отмены нажмите на кнопку', reply_markup=nazad)
    await message.answer('Введите имя',reply_markup=ReplyKeyboardRemove())

# ловим ответ от пользователя

# @dp.message_handler(state=FSMRegistartion.number)
async def load_FName(message: types.Message, state: FSMContext):
    if(message.text.isalpha()==True):
        async with state.proxy() as data:
            data['FName'] = message.text
        await FSMRegUser.next()
        await message.answer('Для отмены нажмите на кнопку', reply_markup=nazad)
        await message.answer('Введите фамилию')
    else:
        await message.answer('Неверно \nВведите имя')
        await FSMRegUser.FName.set()


async def load_SName(message: types.Message, state: FSMContext):
    if (message.text.isalpha()==True):
        async with state.proxy() as data:
            data['SName'] = message.text
        await FSMRegUser.next()
        await message.answer('Для отмены нажмите на кнопку', reply_markup=nazad)
        await message.answer('Введите отчество или пропустите его', reply_markup=kb_skip)
    else:
        await message.answer('Неверно \nВведите фамилию')
        await FSMRegUser.SName.set()


async def load_LName(message: types.Message, state: FSMContext):
    if(message.text == "Пропустить"):
        async with state.proxy() as data:
            data['LName'] = " "
        await message.answer('Для отмены нажмите на кнопку', reply_markup=nazad)
        await message.answer('Введите номер телефона в формате 996*********',reply_markup=ReplyKeyboardRemove())
        await FSMRegUser.Number.set()
    else:
        if (message.text.isalpha()==True):
            async with state.proxy() as data:
                data['LName'] = message.text
            await FSMRegUser.next()
            await message.answer('Для отмены нажмите на кнопку', reply_markup=nazad)
            await message.answer('Введите номер телефона в формате 996*********',reply_markup=ReplyKeyboardRemove())
        else:
            await message.answer('Неверно \nВведите отчество')
            await FSMRegUser.LName.set()



async def load_Number(message: types.Message, state: FSMContext):
    if (message.text.isdigit()==True and len(str(message.text)) == 12 and str(message.text)[0] == "9" and str(message.text)[1] == "9" and str(message.text)[2] == "6"):
        async with state.proxy() as data:
            data['Number'] = message.text
        await FSMRegUser.next()
        await message.answer('Для отмены нажмите на кнопку', reply_markup=nazad)
        await message.answer('Используется ли данный номер в Telegram:', reply_markup=value)
    else:
        await message.answer('Неверно \nВведите номер')
        await FSMRegUser.Number.set()

@dp.message_handler(state=FSMRegUser.Telegram)
async def load_Telegram(message: types.Message, state: FSMContext):
    if(message.text == "Да"):
        async with state.proxy() as data:
            data['Number_Telegram'] = data['Number']
        await message.answer("Используется ли этот номер в Whatsapp", reply_markup=value)
        await FSMRegUser.Whatsapp.set()
    elif(message.text == "Нет"):
        async with state.proxy() as data:
            data['Number_Telegram'] = " "
        await FSMRegUser.Whatsapp.set()
        await message.answer("Используется ли этот номер в Whatsapp", reply_markup=value)

@dp.message_handler(state=FSMRegUser.Whatsapp)
async def load_Whatsapp(message: types.Message, state: FSMContext):
    if (message.text == "Да"):
        async with state.proxy() as data:
            data['Number_WhatsApp'] = data['Number']
        await FSMRegUser.Photo.set()
        await message.answer('Для отмены нажмите на кнопку', reply_markup=nazad)
        await message.answer('Загрузите фото клиента', reply_markup=ReplyKeyboardRemove())
    elif (message.text == "Нет"):
        await message.answer("Введите номер телефона для Whatsapp в формате 996*********")
        await FSMRegUser.Number_WhatsApp.set()

@dp.message_handler(state=FSMRegUser.Number_WhatsApp)
async def load_Number_WhatsApp(message: types.Message, state: FSMContext):
    if (message.text.isdigit()==True and len(str(message.text)) == 12 and str(message.text)[0] == "9" and str(message.text)[1] == "9" and str(message.text)[2] == "6"):
        async with state.proxy() as data:
            data['Number_WhatsApp'] = message.text
        await FSMRegUser.next()
        await message.answer('Для отмены нажмите на кнопку', reply_markup=nazad)
        await message.answer('Загрузите фото клиента', reply_markup=ReplyKeyboardRemove())
    else:
        await message.answer('Неверно \nВведите номер для Whatsapp в формате 996*********')
        await FSMRegUser.Number.set()



@dp.message_handler(content_types = ['photo'] , state=FSMRegUser.Photo)
async def load_Photo(message: types.Message, state: FSMContext):
    async with state.proxy() as data:
        data['Photo'] = message.photo[0].file_id
    my_clients_test = ""
    my_clients = f""" SELECT PASSPORT_SERIES FROM series_passport"""
    connection = Date_base.create_connection()
    with connection.cursor() as cursor:
        cursor.execute(my_clients)
        for my_clients_list in cursor.fetchall():
            my_clients_test = my_clients_test + (f" {my_clients_list[0]}")
    a = my_clients_test.split(' ')
    a.pop(0)
    print(a)
    keyboard = ReplyKeyboardMarkup(resize_keyboard=True, one_time_keyboard=True).row(
        *[KeyboardButton(text=x) for x in a])
    await message.answer('Для отмены нажмите на кнопку', reply_markup=nazad)
    await FSMRegUser.next()
    await message.answer("Введите серию паспорта", reply_markup=keyboard)

@dp.message_handler(state=FSMRegUser.PassportSeries)
async def load_PassportSeries(message: types.Message, state: FSMContext):
    async with state.proxy() as data:
        data['PassportSeries'] = message.text
    await FSMRegUser.next()
    await message.answer('Для отмены нажмите на кнопку', reply_markup=nazad)
    await message.answer("Введите номер паспорта")


async def load_PassportID(message: types.Message, state: FSMContext):
    async with state.proxy() as data:
        data['PassportID'] = message.text
    await FSMRegUser.next()
    await message.answer('Для отмены нажмите на кнопку', reply_markup=nazad)
    await message.answer('Введте ИНН')



async def load_INN(message: types.Message, state: FSMContext):
    if (message.text.isalnum()==True):
        async with state.proxy() as data:
            data['INN'] = message.text
        await FSMRegUser.next()
        await message.answer('Для отмены нажмите на кнопку', reply_markup=nazad)
        await message.answer('Введите месяц и год окончание срока паспорта в формате мм.гггг')
    else:
        await message.answer('Неверно \nВведите ИНН')
        await FSMRegUser.INN.set()

async def load_PassportDate(message: types.Message, state: FSMContext):
        async with state.proxy() as data:
            data['PassportDate'] = message.text
        await FSMRegUser.next()
        await message.answer('Загрузите фото первой страницы паспорта')

@dp.message_handler(content_types = ['photo'] , state=FSMRegUser.PhotoPassOne)
async def load_PhotoPassOne(message: types.Message, state: FSMContext):
    async with state.proxy() as data:
        data['PhotoPassOne'] = message.photo[2].file_id
    await FSMRegUser.next()
    await message.answer('Для отмены нажмите на кнопку', reply_markup=nazad)
    await message.answer('Загрузите фото второй страницы паспорта')

@dp.message_handler(content_types = ['photo'] , state=FSMRegUser.PhotoPassTwo)
async def load_PhotoPassTwo(message: types.Message, state: FSMContext):
    async with state.proxy() as data:
        data['PhotoPassTwo'] = message.photo[0].file_id
    await FSMRegUser.next()
    await message.answer('Для отмены нажмите на кнопку', reply_markup=nazad)
    await message.answer('Введите адресс прописки')

async def load_AdressBorn(message: types.Message, state: FSMContext):
    async with state.proxy() as data:
        data['AdressBorn'] = message.text
    await FSMRegUser.next()
    await message.answer('Для отмены нажмите на кнопку', reply_markup=nazad)
    await message.answer('Введите адресс фактического проживания(сейчас)')


async def load_AdressNow(message: types.Message, state: FSMContext):
    async with state.proxy() as data:
        data['AdressNow'] = message.text
    await FSMRegUser.next()
    await message.answer('Для отмены нажмите на кнопку', reply_markup=nazad)
    await message.answer('Загрузите фото анкеты, подтверждающей согласие клиента')

@dp.message_handler(content_types = ['photo'] , state=FSMRegUser.AnketPhoto)
async def load_AnketPhoto(message: types.Message, state: FSMContext):
    async with state.proxy() as data:
        data['AnketPhoto'] = message.photo[0].file_id
    await FSMRegUser.Podtverdit.set()
    await message.answer("Для проверки внесенных данных нажмите кнопку 'Проверить'", reply_markup=Dalee)


@dp.message_handler(state=FSMRegUser.Podtverdit)
async def load_Podtverdit(message: types.Message, state: FSMContext):
    async with state.proxy() as data:
        await message.bot.send_photo(message.from_user.id, data['Photo'], f"Имя: {data['FName']}\nФамилия: {data['SName']}\nОтчество: {data['LName']}\nНомер WhatsApp: {data['Number_WhatsApp']}\nНомер Telegram: {data['Number_Telegram']}\nСерия и номер паспорта: {data['PassportSeries'] + ' ' + data['PassportID']}\nИНН: {data['INN']}\nМесяц и год окончания паспорта: {data['PassportDate']}\nМесто рождения: {data['AdressBorn']}\nАдрес проживания сейчас: {data['AdressNow']}")
    await message.answer("Подтвердить или отменить добавление клиента", reply_markup=client_accept)
    await FSMRegUser.next()



#    ID = State()
#    FName = State()
#    SName = State()
#    LName = State()
#    Messenger = State()
#    Number = State()
#    Photo = State()
#    PassportID = State()
#    INN = State()
#    PassportDate = State()
#    PhotoPassOne = State()
#    PhotoPassTwo = State()
#    AdressBorn = State()
#    AdressNow = State()
#    AnketPhoto = State()
#








@dp.message_handler(state=FSMRegUser.Import)
async def load_Podtverdit(message: types.Message, state: FSMContext):
    if(message.text=="Подтвердить"):
        async with state.proxy() as data:
            Date_base.execute_query(f"INSERT INTO client (ID_BUSINESS, NAME_CLIENT, SN_CLIENT, LN_CLIENT, NUMB ,NUMB_PHONE_TELEGRAM, NUMB_PHONE_WHATSAPP, PHOTO_CLIENT, PASS_CLIENT_SERIES,PASS_CLIENT_NUMBER ,INN_CLIENT, PASSDATE_CLIENT, PP1_CLIENT, PP2_CLIENT, ADRESSBORN_CLIENT, ADRESSNOW_CLIENT, ANKET_CLIENT) VALUES ('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')"%(int(data['ID']), str(data['FName']), str(data['SName']), str(data['LName']), int(data['Number']),data['Number_Telegram'] , int(data['Number_WhatsApp']),  str(data['Photo']), str(data['PassportSeries']),str(data['PassportID']), str(data['INN']), str(data['PassportDate']), str(data['PhotoPassOne']), str(data['PhotoPassTwo']), str(data['AdressBorn']), str(data['AdressNow']), str(data['AnketPhoto'])))
        await state.finish()
        await message.answer("Клиент успешно добавлен")
        await message.answer('выберите действие', reply_markup=main_kb)
    elif(message.text=="Отменить"):
        await message.answer("Ваши действия отменены")
        await state.finish()
        await message.answer('выберите действие', reply_markup=main_kb)



@dp.callback_query_handler(state="*", text='назад')
@dp.message_handler(Text(equals='назад', ignore_case=True), state="*")
async def cancel_handler_call(callback : types.CallbackQuery, state: FSMContext):
    current_state = await state.get_state()
    if current_state is None:
        return
    await state.finish()
    await callback.message.reply("OK",reply_markup=ReplyKeyboardRemove())
    await callback.message.answer('выберите действие', reply_markup=main_kb)


def register_handlers_client(dp: Dispatcher):
    dp.register_message_handler(load_ID, state=FSMRegUser.ID)
    dp.register_message_handler(load_FName, state=FSMRegUser.FName)
    dp.register_message_handler(load_SName, state=FSMRegUser.SName)
    dp.register_message_handler(load_LName, state=FSMRegUser.LName)
    dp.register_message_handler(load_Number, state=FSMRegUser.Number)
    dp.register_message_handler(load_Photo, content_types = ['photo'] , state=FSMRegUser.PhotoPassOne)
    dp.register_message_handler(load_PassportID, state=FSMRegUser.PassportID)
    dp.register_message_handler(load_INN, state=FSMRegUser.INN)
    dp.register_message_handler(load_PassportDate, state=FSMRegUser.PassportDate)
    dp.register_message_handler(load_PhotoPassOne, content_types = ['photo'] , state=FSMRegUser.PhotoPassOne)
    dp.register_message_handler(load_PhotoPassTwo, content_types = ['photo'] , state=FSMRegUser.PhotoPassTwo)
    dp.register_message_handler(load_AdressBorn, state=FSMRegUser.AdressBorn)
    dp.register_message_handler(load_AdressNow, state=FSMRegUser.AdressNow)
    dp.register_message_handler(load_AnketPhoto, content_types=['photo'], state=FSMRegUser.AnketPhoto)


