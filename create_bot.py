from typing import Type, Optional

from aiogram import Bot
from aiogram.dispatcher import Dispatcher
from names import token
import os
from aiogram.contrib.fsm_storage.memory import MemoryStorage

storage = MemoryStorage()

bot = Bot(token=token)  # объявляем бота
dp = Dispatcher(bot, storage=storage)
