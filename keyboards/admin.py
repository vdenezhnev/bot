import asyncio
import calendar
import Date_base
from datetime import datetime, timedelta, date
from dateutil.relativedelta import relativedelta

from aiogram import types
from aiogram.dispatcher import FSMContext
from aiogram.dispatcher.filters.state import State, StatesGroup
from aiogram.types import InlineKeyboardMarkup, InlineKeyboardButton, ReplyKeyboardMarkup, KeyboardButton, \
    ReplyKeyboardRemove
from mysql.connector import Error

from create_bot import dp, bot
from keyboards.client import Method_pay, client_accept, keyboard_search
from names import site_link, ref_site_link


inl_kb_1 = InlineKeyboardMarkup(row_width=1)#row_width кол-во кнопок в строке

group = InlineKeyboardButton(text='Ссылка на группу поддержки', url='https://rozetka.com.ua/')
inl_kb_1.row(group)

main_kb = InlineKeyboardMarkup(row_width=3)

shop_kb = InlineKeyboardMarkup(row_width=3)

nazad = InlineKeyboardMarkup(row_width=1)




nazad_but = InlineKeyboardMarkup(text='ОТМЕНА', callback_data='назад')

nazad.row(nazad_but)




b6_1 = InlineKeyboardMarkup(text='Название', callback_data='name_shop')#+
b6_2 = InlineKeyboardMarkup(text='Адрес', callback_data='adres_shop')#+
b6_3 = InlineKeyboardMarkup(text='Номер', callback_data='number_shop')#+
b6_4 = InlineKeyboardMarkup(text='Каталог товаров', callback_data='list_tovar')#+-
b6_5 = InlineKeyboardMarkup(text='Добавить товар', callback_data='add_tovar')#+
b6_6 = InlineKeyboardMarkup(text='Настройка приема оплат для погашения долга', callback_data='oplata')#+-
b6_7 = InlineKeyboardMarkup(text='Ссылка на магазин', callback_data='my_own_shop')
b6_8 = InlineKeyboardMarkup(text='Логотип', callback_data='logo_shop')#+

"""

"""


class FSMlogo(StatesGroup):
    add_logo = State()
    change_logo = State()

@dp.callback_query_handler(text='logo_shop', state=None)#мой магазин
async def dsada_call(callback : types.CallbackQuery):
    logotipe = f""" SELECT * FROM business WHERE ID_BUSINESS = {callback.from_user.id}"""
    connection = Date_base.create_connection()
    with connection.cursor() as cursor:
        cursor.execute(logotipe)
        for logo in cursor.fetchall():
            if len(logo[13]) == 0:
                await callback.message.answer("Всавьте фото с логотипом")
                await FSMlogo.add_logo.set()
            else:
                await callback.bot.send_photo(callback.from_user.id, logo[13])
                await callback.message.answer('Изменить логотип', reply_markup=InlineKeyboardMarkup().add(
                    InlineKeyboardButton(text="Изменить", callback_data='change_logo')))


                @dp.callback_query_handler(text='change_logo')
                async def change_address_call(callback: types.CallbackQuery):
                    await callback.message.answer("Вставьте новый логотип")
                    await FSMlogo.change_logo.set()

                @dp.message_handler(state=FSMlogo.change_logo, content_types=['photo'])
                async def load_change(message: types.Message, state: FSMContext):
                    Date_base.execute_query(f"""UPDATE business SET LOGO_PHOTO = '{message.photo[0].file_id}' WHERE ID_BUSINESS = {message.from_user.id}""")
                    await state.finish()
                    await message.answer('Изменения внесены успешно')
                    await message.answer('выберите действие', reply_markup=main_kb)

@dp.message_handler(state=FSMlogo.add_logo, content_types=['photo'])
async def load_change(message: types.Message, state: FSMContext):
    Date_base.execute_query(f"""UPDATE business SET LOGO_PHOTO = '{message.photo[0].file_id}' WHERE ID_BUSINESS = {message.from_user.id}""")
    await state.finish()
    await message.answer('Операция прошла успешно')
    await message.answer('выберите действие', reply_markup=main_kb)

b3_1 = InlineKeyboardMarkup(text='Проверить человека', callback_data='check_user', commands = ['check_user'])#+-
b3_2 = InlineKeyboardMarkup(text='Добавить клиента', callback_data='add_user')#+
b3_3 = InlineKeyboardMarkup(text='Мои клиенты', callback_data='/my_clients')#+-
b3_4 = InlineKeyboardMarkup(text='Добавить долг', callback_data='dolg')#+-
b3_5 = InlineKeyboardMarkup(text='Мой магазин', callback_data='my_shop')#+
b3_6 = InlineKeyboardMarkup(text='Мой баланс', callback_data='my_balance')#+
b3_7 = InlineKeyboardMarkup(text='Мой профиль', callback_data='own_user')#+
b3_8 = InlineKeyboardMarkup(text='FAQ', callback_data='questions')#+
b3_9 = InlineKeyboardMarkup(text='Пригласить друга', callback_data='add_friend')#+
b3_10 = InlineKeyboardMarkup(text='Поддержка', callback_data='help')#+
b3_11 = InlineKeyboardMarkup(text='Активные долги', callback_data='a_orders')

main_kb.row(b3_1,b3_2).row(b3_3,b3_4,b3_11).row(b3_5,b3_6,b3_7).row(b3_8,b3_9,b3_10)
shop_kb.row(b6_1,b6_2,b6_8).row(b6_3,b6_4).row(b6_5,b6_7).row(b6_6)

class FSMorders(StatesGroup):
    info = State()
    anketa = State()


@dp.callback_query_handler(text='a_orders')#мой магазин
async def a_orders_call(callback : types.CallbackQuery):
    orders = f""" SELECT * FROM orders WHERE ID_BUSINESS = {callback.from_user.id} AND PRICE_PRODUCT > EVERYMONTH_PRICE"""
    connection = Date_base.create_connection()
    with connection.cursor() as cursor:
        cursor.execute(orders)
        for order in cursor.fetchall():
            clients = f""" SELECT * FROM client WHERE ID_CLIENT = {order[1]}"""
            with connection.cursor() as cursor:
                cursor.execute(clients)
                for client in cursor.fetchall():
                    await callback.message.answer(f"{client[2]} {client[3]} {order[7]} сомов из {order[6]} сомов")
                    await callback.message.answer("Выберите действие", reply_markup=InlineKeyboardMarkup().row(
                        InlineKeyboardButton(text="Анкета клиента", callback_data=f'anketa {order[1]}'),InlineKeyboardButton(text="Информация", callback_data=f'info {order[0]}')))
        await callback.message.answer('выберите действие', reply_markup=main_kb)




@dp.callback_query_handler(lambda x: (x.data and x.data.startswith('anketa ')) or (x.data and x.data.startswith('info '))) # анкета клиента и информация
async def dolg_call(callback: types.CallbackQuery, state: FSMContext):
    a = (callback.data).split(' ')
    print(a[1])
    if (a[0] == 'anketa'):
        my_clients = f""" SELECT * FROM client WHERE ID_CLIENT = {a[1]}"""
        connection = Date_base.create_connection()
        with connection.cursor() as cursor:
            cursor.execute(my_clients)
            for my_clients_list in cursor.fetchall():
                await callback.bot.send_photo(callback.from_user.id, my_clients_list[8],
                                              f"Имя: {my_clients_list[2]}\nФамилия: {my_clients_list[3]}\nОтчество: {my_clients_list[4]}\nНомер телефона Telegram: {my_clients_list[6]}\nНомер телефона WhatsApp: {my_clients_list[7]}")
        await callback.message.answer('выберите действие', reply_markup=main_kb)
    elif (a[0] == 'info'):
        infos = f""" SELECT * FROM orders WHERE ID_ORDER = {a[1]}"""
        connection = Date_base.create_connection()
        with connection.cursor() as cursor:
            cursor.execute(infos)
            for inform in cursor.fetchall():
                prods = f""" SELECT * FROM product WHERE ID_PRODUCT = {inform[3]}"""
                with connection.cursor() as cursor:
                    cursor.execute(prods)
                    for prod in cursor.fetchall():
                        await callback.message.answer(f"Название товара: {prod[3]}\nДата оформления заказа: {inform[9]}\nЦена товара: {inform[6]}\nКоличество месяцев рассрочки: {inform[8]}\nСумма ежемесячной рассрочки: {prod[8]}\n")
        await callback.message.answer('выберите действие', reply_markup=main_kb)

#В разделе информация указана информация платежа (название товара, Дата оформления заказа, цена товара, процент расточки, количество месяцев рассрочки, сумма ежемесячной рассрочки







add_tovar = InlineKeyboardMarkup(row_width=1).row(b6_5)

@dp.callback_query_handler(text='my_shop')#мой магазин
async def dsada_call(callback : types.CallbackQuery):
    await callback.message.answer("Мой магазин", reply_markup=shop_kb)


@dp.callback_query_handler(text='my_balance')#мой баланс
async def my_balance_call(callback: types.CallbackQuery):
    monets = f""" SELECT * FROM business WHERE ID_BUSINESS = {callback.from_user.id}"""
    connection = Date_base.create_connection()
    with connection.cursor() as cursor:
        cursor.execute(monets)
        for monets in cursor.fetchall():
            await callback.message.answer(f"Ваш баланс {monets[8]} монет")


@dp.callback_query_handler(text='own_user')#мой профиль
async def profile_names_call(callback: types.CallbackQuery):
    monets = f""" SELECT * FROM business WHERE ID_BUSINESS = {callback.from_user.id}"""
    connection = Date_base.create_connection()
    with connection.cursor() as cursor:
        cursor.execute(monets)
        for profile_names in cursor.fetchall():
            await callback.message.answer(
                f"Имя: {profile_names[1]}\nФамилия: {profile_names[2]}\nКоличество монет: {profile_names[8]}\nНазвание магазина: {profile_names[5]}")

@dp.callback_query_handler(text='questions')#FAQ
async def questions_call(callback: types.CallbackQuery):
    await callback.message.answer( "Часто задаваемые вопросы:\n1)Как добавить пользователя?\n1.Нужно перейти в меню и нажать команду 'Добавить пользователя' или просто ввести 'Добавить пользователя'")


@dp.callback_query_handler(text='name_shop')#название магазина
async def name_shop_call(callback: types.CallbackQuery):
    name_shop = f""" SELECT * FROM business WHERE ID_BUSINESS = {callback.from_user.id}"""
    connection = Date_base.create_connection()
    with connection.cursor() as cursor:
        cursor.execute(name_shop)
        for name_shop_1 in cursor.fetchall():
            await callback.message.answer(f"Название магазина: {name_shop_1[5]}")
            await callback.message.answer('Изменить название магазина', reply_markup=InlineKeyboardMarkup().add(
                InlineKeyboardButton(text="Изменить", callback_data='change_name_shop')))

        class FSMchange(StatesGroup):
            change_2 = State()

        @dp.callback_query_handler(text='change_name_shop')
        async def change_address_call(callback: types.CallbackQuery):
            await callback.message.answer("Введите новое название магазина")
            await FSMchange.change_2.set()

        @dp.message_handler(state=FSMchange.change_2)
        async def load_change(message: types.Message, state: FSMContext):
            Date_base.execute_query(f"""UPDATE business SET NAME_OF_BUSINESS = '{str(message.text)}' WHERE ID_BUSINESS = {message.from_user.id}""")
            await state.finish()
            await message.answer('Изменения внесены успешно')
            await message.answer('выберите действие', reply_markup=main_kb)

@dp.callback_query_handler(text='number_shop')#номер магазина
async def number_shop_call(callback: types.CallbackQuery):
    name_shop = f""" SELECT * FROM business WHERE ID_BUSINESS = {callback.from_user.id}"""
    connection = Date_base.create_connection()
    with connection.cursor() as cursor:
        cursor.execute(name_shop)
        for name_shop_1 in cursor.fetchall():
            await callback.message.answer(f"Номер телефона магазина: {name_shop_1[3]}")
            await callback.message.answer('Изменить номер магазина', reply_markup=InlineKeyboardMarkup().add(InlineKeyboardButton(text="Изменить", callback_data='change_number')))

class FSMchange(StatesGroup):
    change_1 = State()

@dp.callback_query_handler(text='change_number')
async def change_address_call(callback: types.CallbackQuery):
    await callback.message.answer("Введите новый номер магазина в формате 996*********")
    await FSMchange.change_1.set()

@dp.message_handler(state=FSMchange.change_1)
async def load_change(message: types.Message, state: FSMContext):
    if(message.text.isdigit()==True and len(str(message.text)) == 12 and str(message.text)[0] == "9" and str(message.text)[1] == "9" and str(message.text)[2] == "6"):
        Date_base.execute_query(f"""UPDATE business SET NUMBER_BUSINESS = '{str(message.text)}' WHERE ID_BUSINESS = {message.from_user.id}""")
        await state.finish()
        await message.answer('Изменения внесены успешно')
        await message.answer('выберите действие', reply_markup=main_kb)
    else:
        await message.answer("Неверно\nВведите правильны номер в формате 996*********")
        await FSMchange.change_1.set()


@dp.callback_query_handler(text='adres_shop')#адрес магазина
async def adres_shop_call(callback: types.CallbackQuery):
    name_shop = f""" SELECT * FROM business WHERE ID_BUSINESS = {callback.from_user.id}"""
    connection = Date_base.create_connection()
    with connection.cursor() as cursor:
        cursor.execute(name_shop)
        for name_shop_1 in cursor.fetchall():
            await callback.message.answer(f"Адрес магазина: {name_shop_1[6]}")
            await callback.message.answer('Изменить адрес магазина', reply_markup=InlineKeyboardMarkup().add(
                InlineKeyboardButton(text="Изменить", callback_data='change_address')))

        class FSMchange(StatesGroup):
            change_3 = State()

        @dp.callback_query_handler(text='change_address')
        async def change_address_call(callback: types.CallbackQuery):
            await callback.message.answer("Введите новый адрес магазина")
            await FSMchange.change_3.set()

        @dp.message_handler(state=FSMchange.change_3)
        async def load_change(message: types.Message, state: FSMContext):
                        Date_base.execute_query(f"""UPDATE business SET ADRESS_BUSINESS = '{str(message.text)}' WHERE ID_BUSINESS = {message.from_user.id}""")
                        await state.finish()
                        await message.answer('Изменения внесены успешно')
                        await message.answer('выберите действие', reply_markup=main_kb)



@dp.callback_query_handler(text='add_friend')  # адрес магазина
async def add_friend_call(callback: types.CallbackQuery):
    await callback.message.answer(f"Реферальная ссылка для приглашения \n{ref_site_link+str(callback.from_user.id)}")


@dp.callback_query_handler(text='help')  # адрес магазина
async def help_call(callback: types.CallbackQuery):
    await callback.message.answer("Группа, куда вы можете отправить интересующий вас вопрос:\n", reply_markup=inl_kb_1)

@dp.callback_query_handler(text='my_own_shop')  # адрес магазина
async def my_own_shop_call(callback: types.CallbackQuery):
    my_own_shop = f""" SELECT * FROM business WHERE ID_BUSINESS = {callback.from_user.id}"""
    connection = Date_base.create_connection()
    with connection.cursor() as cursor:
        cursor.execute(my_own_shop)
        for my_own_shop_link in cursor.fetchall():
            await callback.message.answer(f"Ссылка на магазин \n{site_link+str(my_own_shop_link[7])}")
            await callback.message.answer('Изменить ссылку на магазин', reply_markup=InlineKeyboardMarkup().add(
                InlineKeyboardButton(text="Изменить", callback_data='change_link')))

        class FSMchange(StatesGroup):
            change = State()

        @dp.callback_query_handler(text='change_link')
        async def change_address_call(callback: types.CallbackQuery):
            await callback.message.answer("Введите новую ссылку на магазин")
            await FSMchange.change.set()

        @dp.message_handler(state=FSMchange.change)
        async def load_change(message: types.Message, state: FSMContext):
            query_adm = f"SELECT  COUNT(LINK_BUSINESS) FROM business WHERE LINK_BUSINESS='{str(message.text)}'"
            connection = Date_base.create_connection()
            with connection.cursor() as cursor:
                cursor.execute(query_adm)
                for my_clients_list in cursor.fetchall():
                    a = my_clients_list[0]
                    #print(a)
                    if (a == 0):
                        Date_base.execute_query(query = f"""UPDATE business SET LINK_BUSINESS = '{str(message.text)}' WHERE ID_BUSINESS = {message.from_user.id}""")
                        await state.finish()
                        await message.answer('Изменения внесены успешно')
                        await message.answer('выберите действие', reply_markup=main_kb)
                    else:
                        await message.answer("Данная сылка уже используется. Введите другую ссылку")
                        await FSMchange.change.set()



@dp.callback_query_handler(text='list_tovar') # список товаров
async def list_tovar_call(callback: types.CallbackQuery):
    all_tovar = f""" SELECT * FROM product WHERE ID_BUSINESSMAN = {callback.from_user.id}"""
    connection = Date_base.create_connection()
    with connection.cursor() as cursor:
        cursor.execute(all_tovar)
        for all_tovar_list in cursor.fetchall():
            await callback.bot.send_photo(callback.from_user.id, all_tovar_list[9], f"Категория товара: {all_tovar_list[2]}\nНазвание продукта: {all_tovar_list[3]}\nСебестоимость: {all_tovar_list[4]}\nЦена на продажу: {all_tovar_list[5]} ")

    await callback.message.answer('выберите действие', reply_markup=main_kb)

class FSMOplata(StatesGroup):
    ID = State()
    Method_of_pay = State()
    Card = State()
    ID_Paybox = State()
    Token_Paybox = State()
    elsom = State()
    OMoney = State()
    Balance = State()
    MegaPay = State()

@dp.callback_query_handler(text='oplata') # оплата
async def oplata_call(callback: types.CallbackQuery):
    await FSMOplata.ID.set()
    await callback.message.answer('Выберите метод оплаты', reply_markup=Method_pay)

@dp.message_handler(state=FSMOplata.ID)
async def load_ID(message: types.Message, state: FSMContext):
    if(message.text == "Paybox"):
        async with state.proxy() as data:
            data['Method_of_pay'] = message.text
        await message.answer("Введите данные для ID Paybox")
        await FSMOplata.ID_Paybox.set()
    elif(message.text == "Картой OptimaBank"):
        async with state.proxy() as data:
            data['Method_of_pay'] = message.text
        await message.answer("Введите данные карты")
        await FSMOplata.Card.set()
    elif(message.text == "Кошелек Элсом"):
        async with state.proxy() as data:
            data['Method_of_pay'] = message.text
        await message.answer("Введите номер телефона для кошелька Элсом")
        await FSMOplata.elsom.set()
    elif(message.text == "Кошелек О!Деньги"):
        async with state.proxy() as data:
            data['Method_of_pay'] = message.text
        await message.answer("Введите номер телефона для кошелька О!Деньги")
        await FSMOplata.OMoney.set()
    elif(message.text == "Кошелек Balance"):
        async with state.proxy() as data:
            data['Method_of_pay'] = message.text
        await message.answer("Введите номер телефона для кошелька Balance")
        await FSMOplata.Balance.set()
    elif(message.text == "Кошелек MegaPay"):
        async with state.proxy() as data:
            data['Method_of_pay'] = message.text
        await message.answer("Введите номер телефона для кошелька MegaPay")
        await FSMOplata.MegaPay.set()



@dp.message_handler(state=FSMOplata.elsom)
async def load_ID_Paybox(message: types.Message, state: FSMContext):
    if(message.text.isdigit()==True and len(str(message.text)) == 12 and str(message.text)[0] == "9" and str(message.text)[1] == "9" and str(message.text)[2] == "6"):
        async with state.proxy() as data:
            Date_base.execute_query(f"UPDATE business SET METHOD_OF_PAY = '{str(data['Method_of_pay'])}', WALLET_ELSOM = {str(message.text)} WHERE ID_BUSINESS = {int(message.from_user.id)}")
        await state.finish()
        await message.answer('Способ оплаты успешно добавлен', reply_markup=ReplyKeyboardRemove())
        await message.answer('выберите действие', reply_markup=main_kb)
    else:
        await message.answer("Неверно\nВведите правильный номер телефона")



@dp.message_handler(state=FSMOplata.OMoney)
async def load_ID_Paybox(message: types.Message, state: FSMContext):
    if(message.text.isdigit()==True and len(str(message.text)) == 12 and str(message.text)[0] == "9" and str(message.text)[1] == "9" and str(message.text)[2] == "6"):
        async with state.proxy() as data:
            Date_base.execute_query(f"UPDATE business SET METHOD_OF_PAY = '{str(data['Method_of_pay'])}', WALLET_ODENGI = {str(message.text)} WHERE ID_BUSINESS = {int(message.from_user.id)}")
        await state.finish()
        await message.answer('Способ оплаты успешно добавлен', reply_markup=ReplyKeyboardRemove())
        await message.answer('выберите действие', reply_markup=main_kb)
    else:
        await message.answer("Неверно\nВведите правильный номер телефона")

@dp.message_handler(state=FSMOplata.Balance)
async def load_ID_Paybox(message: types.Message, state: FSMContext):
    if(message.text.isdigit()==True and len(str(message.text)) == 12 and str(message.text)[0] == "9" and str(message.text)[1] == "9" and str(message.text)[2] == "6"):
        async with state.proxy() as data:
            Date_base.execute_query(query = f"UPDATE business SET METHOD_OF_PAY = '{str(data['Method_of_pay'])}', WALLET_BALANCE = {str(message.text)} WHERE ID_BUSINESS = {int(message.from_user.id)}")
        await state.finish()
        await message.answer('Способ оплаты успешно добавлен', reply_markup=ReplyKeyboardRemove())
        await message.answer('выберите действие', reply_markup=main_kb)
    else:
        await message.answer("Неверно\nВведите правильный номер телефона")

@dp.message_handler(state=FSMOplata.MegaPay)
async def load_ID_Paybox(message: types.Message, state: FSMContext):
    if(message.text.isdigit()==True and len(str(message.text)) == 12 and str(message.text)[0] == "9" and str(message.text)[1] == "9" and str(message.text)[2] == "6"):
        async with state.proxy() as data:
            Date_base.execute_query(f"UPDATE business SET METHOD_OF_PAY = '{str(data['Method_of_pay'])}', WALLET_MEGAPAY = {str(message.text)} WHERE ID_BUSINESS = {int(message.from_user.id)}")
        await state.finish()
        await message.answer('Способ оплаты успешно добавлен', reply_markup=ReplyKeyboardRemove())
        await message.answer('выберите действие', reply_markup=main_kb)
    else:
        await message.answer("Неверно\nВведите правильный номер телефона")




@dp.message_handler(state=FSMOplata.Card)
async def load_ID_Paybox(message: types.Message, state: FSMContext):
    if(message.text.isdigit() == True):
        async with state.proxy() as data:
            Date_base.execute_query(f"UPDATE business SET METHOD_OF_PAY = '{str(data['Method_of_pay'])}', PAY_CARD = {str(message.text)} WHERE ID_BUSINESS = {int(message.from_user.id)}")
        await state.finish()
        await message.answer('выберите действие', reply_markup=main_kb)
    else:
        await message.answer("Неверно\nВведите правильный номер карты")



@dp.message_handler(state=FSMOplata.ID_Paybox)
async def load_ID_Paybox(message: types.Message, state: FSMContext):
    async with state.proxy() as data:
        data['ID_Paybox'] = message.text
    await message.answer("Введите данные для Token Paybox")
    await FSMOplata.Token_Paybox.set()

    @dp.message_handler(state=FSMOplata.Token_Paybox)
    async def load_ID_Paybox(message: types.Message, state: FSMContext):
        async with state.proxy() as data:
            data['Token_Paybox'] = message.text

        async with state.proxy() as data:
            Date_base.execute_query(f"UPDATE business SET METHOD_OF_PAY = '{str(data['Method_of_pay'])}', ID_PAYBOX = '{str(data['ID_Paybox'])}', TOKEN_PAYBOX='{str(data['Token_Paybox'])}' WHERE ID_BUSINESS = {int(message.from_user.id)}")
            #print(query)
        await state.finish()
        await message.answer('выберите действие', reply_markup=main_kb)



class FSM_check_user(StatesGroup):
        serch_param = State()
        input_serch_FIO = State()
        User_FN = State()
        User_SN = State()
        User_LN = State()
        input_serch_Number = State()
        input_serch_INN = State()
        list_users_FIO = State()
        list_users_Number = State()
        list_users_INN = State()
        loading_ID = State()
        PhotoPass_MyClients = State()


def idshnick(id):
    business = f""" SELECT NAME_BUSINES FROM business WHERE ID_BUSINESS  = '{id}'"""
    connection = Date_base.create_connection()
    with connection.cursor() as cursor:
        cursor.execute(business)
        for business_list in cursor.fetchall():
            return business_list[0]


@dp.callback_query_handler(text='check_user') # проверить пользователя
async def check_user_call(callback: types.CallbackQuery):
    await callback.message.answer('Выберите параметр поиска клиента', reply_markup=keyboard_search)
    await FSM_check_user.serch_param.set()

@dp.message_handler(state=FSM_check_user.serch_param)
async def load_serch_param(message: types.Message, state: FSMContext):
    if(message.text == "По ФИО"):
        await message.answer("Введите ФИО пользователя")
        await FSM_check_user.input_serch_FIO.set()
    elif(message.text == "По номеру телефона"):
        await message.answer("Введите номер телефона пользователя в формате 996*********")
        await FSM_check_user.input_serch_Number.set()
    elif(message.text == "По ИНН"):
        await message.answer("Введите ИНН пользователя")
        await FSM_check_user.input_serch_INN.set()


@dp.message_handler(state=FSM_check_user.input_serch_FIO)
async def load_input_serch_FIO(message: types.Message, state: FSMContext):
    a = message.text.split(' ')
    #print(a[0])
    #print(len(a))
    if(len(a) == 3):
        my_clients = f""" SELECT DISTINCT INN_CLIENT, NAME_CLIENT,SN_CLIENT,LN_CLIENT,ID_CLIENT,NUMB FROM client WHERE SN_CLIENT = '{str(a[0])}' AND NAME_CLIENT = '{str(a[1])}' AND LN_CLIENT = '{str(a[2])}'"""
        #print(len(a))
        connection = Date_base.create_connection()
        with connection.cursor() as cursor:
            cursor.execute(my_clients)
            for my_clients_list in cursor.fetchall():
                async with state.proxy() as data:
                    data['input_serch_INN'] = my_clients_list[0]
                if (int(my_clients_list[0]) != 0):
                    await message.answer(f"Имя: {my_clients_list[1]}\nФамилия: {my_clients_list[2]}\nОтчество: {my_clients_list[3]}\nНомер телефона: {my_clients_list[5]}\nИНН: {my_clients_list[0]} ")
                    await message.answer("^^^", reply_markup=InlineKeyboardMarkup().add(
                        InlineKeyboardButton(text="Подробнее", callback_data=f'select_tovar {my_clients_list[0]}')))
                    await FSM_check_user.loading_ID.set()
                else:
                    await message.answer("Такой пользователь не совершал покупки")
                    await state.finish()
                    await message.answer('выберите действие', reply_markup=main_kb)
    elif(len(a) == 2):
        my_clients = f""" SELECT DISTINCT INN_CLIENT, NAME_CLIENT, SN_CLIENT, LN_CLIENT, ID_CLIENT, NUMB FROM client WHERE SN_CLIENT = '{str(a[0])}' AND NAME_CLIENT = '{str(a[1])}' """
        connection = Date_base.create_connection() 
        with connection.cursor() as cursor:
            cursor.execute(my_clients)
            if (cursor.arraysize != 0):
                for my_clients_list in cursor.fetchall():
                    print(my_clients_list)
                    async with state.proxy() as data:
                        data['input_serch_INN'] = my_clients_list[0]
                    await message.answer(f"Имя: {my_clients_list[1]}\nФамилия: {my_clients_list[2]}\nОтчество: {my_clients_list[3]}\nНомер телефона: {my_clients_list[5]}\nИНН: {my_clients_list[0]} ")
                    await message.answer("^^^", reply_markup=InlineKeyboardMarkup().add(
                        InlineKeyboardButton(text="Подробнее", callback_data=f'select_tovar {my_clients_list[0]}')))
                    await FSM_check_user.loading_ID.set()
            else:
                await message.answer("Такой пользователь не совершал покупки")
                await state.finish()
                await message.answer('выберите действие', reply_markup=main_kb)

#DISTINCT

@dp.message_handler(state=FSM_check_user.input_serch_Number)
async def load_input_serch_Number(message: types.Message, state: FSMContext):
    if (message.text.isdigit()==True and len(str(message.text)) == 12 and str(message.text)[0] == "9" and str(message.text)[1] == "9" and str(message.text)[2] == "6"):
            #print(message.text)
            count_client = f""" SELECT  COUNT(DISTINCT NUMB) FROM client WHERE NUMB = {message.text}"""
            connection = Date_base.create_connection()
            with connection.cursor() as cursor:
                cursor.execute(count_client)
                for count_client_list in cursor.fetchall():
                    if (int(count_client_list[0]) != 0):
                        my_clients = f""" SELECT DISTINCT INN_CLIENT, NAME_CLIENT,SN_CLIENT,LN_CLIENT,ID_CLIENT,NUMB FROM client WHERE NUMB = {str(message.text)}"""
                        with connection.cursor() as cursor:
                            cursor.execute(my_clients)
                            for my_clients_list in cursor.fetchall():
                                async with state.proxy() as data:
                                    data['input_serch_INN'] = my_clients_list[0]
                                if (int(my_clients_list[0]) != 0):
                                    await message.answer(f"Имя: {my_clients_list[1]}\nФамилия: {my_clients_list[2]}\nОтчество: {my_clients_list[3]}\nНомер телефона: {my_clients_list[5]}\nИНН: {my_clients_list[0]} ")
                                    await message.answer("^^^", reply_markup=InlineKeyboardMarkup().add(
                                        InlineKeyboardButton(text="Подробнее", callback_data=f'select_tovar {my_clients_list[0]}')))
                                    await FSM_check_user.loading_ID.set()
                    else:
                        await message.answer("Такой пользователь не совершал покупки")
                        await state.finish()
                        await message.answer('выберите действие', reply_markup=main_kb)
    else:
        await message.answer('Неверно \nВведите номер')
        await FSM_check_user.input_serch_Number.set()


@dp.message_handler(state=FSM_check_user.input_serch_INN)
async def load_input_serch_INN(message: types.Message, state: FSMContext):
    if (message.text.isdigit() == True):
        async with state.proxy() as data:
            data['input_serch_INN'] = message.text
            count_client = f""" SELECT  COUNT(DISTINCT INN_CLIENT) FROM client WHERE INN_CLIENT = {data['input_serch_INN']}"""
            connection = Date_base.create_connection()
            with connection.cursor() as cursor:
                cursor.execute(count_client)
                for count_client_list in cursor.fetchall():
                    if(int(count_client_list[0]) != 0):
                        my_clients = f""" SELECT DISTINCT INN_CLIENT, NAME_CLIENT,SN_CLIENT,LN_CLIENT,ID_CLIENT,NUMB FROM client WHERE INN_CLIENT = {data['input_serch_INN']}"""
                        with connection.cursor() as cursor:
                            cursor.execute(my_clients)
                            for my_clients_list in cursor.fetchall():
                                await message.answer(f"Имя: {my_clients_list[1]}\nФамилия: {my_clients_list[2]}\nОтчество: {my_clients_list[3]}\nНомер телефона: {my_clients_list[5]}\nИНН: {my_clients_list[0]} ")
                                await message.answer("^^^", reply_markup=InlineKeyboardMarkup().add(
                                    InlineKeyboardButton(text="Подробнее", callback_data=f'select_tovar {my_clients_list[0]}')))
                        await FSM_check_user.loading_ID.set()
                    else:
                        await message.answer("Такой пользователь не совершал покупки")
                        await state.finish()
                        await message.answer('выберите действие', reply_markup=main_kb)

    else:
        await message.answer('Неверно \nВведите ИНН')
        await FSM_check_user.input_serch_INN.set()

@dp.callback_query_handler(state=FSM_check_user.loading_ID)
async def callback_ran(callback: types.CallbackQuery, state: FSMContext):
    #print(str(callback.data.replace('select_tovar ', "")))
    my_clients = f""" SELECT * FROM client WHERE INN_CLIENT = '{str(callback.data.replace('select_tovar ', ""))}' LIMIT 1"""
    connection = Date_base.create_connection()
    with connection.cursor() as cursor:
        cursor.execute(my_clients)
        for my_clients_list in cursor.fetchall():
            await callback.bot.send_photo(callback.from_user.id, my_clients_list[8],
                                              f"Имя: {my_clients_list[2]}\nФамилия: {my_clients_list[3]}\nОтчество: {my_clients_list[4]}\nИНН: {my_clients_list[11]}\nАдрес прописки: {my_clients_list[15]}\nФактический адрес: {my_clients_list[16]}",reply_markup=ReplyKeyboardMarkup(resize_keyboard=True, one_time_keyboard=True).row(KeyboardButton("Добавить в мои клиенты"),KeyboardButton("Показать фото паспорта"),KeyboardButton("Главное меню")))
    my_credits = f""" SELECT * FROM orders WHERE INN_CLIENT = '{str(callback.data.replace('select_tovar ', ""))}'"""
    connection = Date_base.create_connection()
    with connection.cursor() as cursor:
        cursor.execute(my_credits)
        for my_credits_list in cursor.fetchall():
                if(my_credits_list[6] >= my_credits_list[7]):
                    a="погашен"
                else:
                    a="не погашен"
                await callback.message.answer(f"{my_credits_list[6]}//{my_credits_list[9]} // {a} // {str(idshnick(my_credits_list[2]))}")
    await FSM_check_user.PhotoPass_MyClients.set()


@dp.message_handler(state=FSM_check_user.PhotoPass_MyClients)
async def load_input_serch_INN(message: types.Message, state: FSMContext):
    if(message.text == "Добавить в мои клиенты"):
        async with state.proxy() as data:
            a = str(data['input_serch_INN'])
            #print(a)
            my_clients = f""" SELECT * FROM client WHERE INN_CLIENT = {a}  LIMIT 1"""
            connection = Date_base.create_connection()
            with connection.cursor() as cursor:
                cursor.execute(my_clients)
                for my_clients_list in cursor.fetchall():
                    Date_base.execute_query(f"INSERT INTO client (ID_BUSINESS, NAME_CLIENT, SN_CLIENT, LN_CLIENT, NUMB ,NUMB_PHONE_TELEGRAM, NUMB_PHONE_WHATSAPP, PHOTO_CLIENT, PASS_CLIENT_SERIES,PASS_CLIENT_NUMBER ,INN_CLIENT, PASSDATE_CLIENT, PP1_CLIENT, PP2_CLIENT, ADRESSBORN_CLIENT, ADRESSNOW_CLIENT, ANKET_CLIENT) VALUES ('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')" % (int(message.from_user.id), str(my_clients_list[2]), str(my_clients_list[3]),str(my_clients_list[4]),str(my_clients_list[5]),str(my_clients_list[6]),str(my_clients_list[7]),str(my_clients_list[8]),str(my_clients_list[9]),str(my_clients_list[10]),str(my_clients_list[11]),str(my_clients_list[12]),str(my_clients_list[13]),str(my_clients_list[14]),str(my_clients_list[15]),str(my_clients_list[16]),str(my_clients_list[17])))
            Date_base.execute_query(f"""UPDATE business SET GOLD_BUSINESS = GOLD_BUSINESS-1 WHERE ID_BUSINESS={message.from_user.id}""")
            await message.answer("Клиент успешно добавлен")
            await state.finish()
            await message.answer('выберите действие', reply_markup=main_kb)
    elif(message.text=="Показать фото паспорта"):
        async with state.proxy() as data:
            a = str(data['input_serch_INN'])
            #print(a)
            my_clients = f""" SELECT * FROM client WHERE INN_CLIENT = {a}  LIMIT 1"""
            connection = Date_base.create_connection()
            with connection.cursor() as cursor:
                cursor.execute(my_clients)
                for my_clients_list in cursor.fetchall():
                    await bot.send_photo(message.from_user.id, my_clients_list[13])
                    await bot.send_photo(message.from_user.id, my_clients_list[14])
                    await message.answer("Вернуться назад", reply_markup=InlineKeyboardMarkup().add(InlineKeyboardButton(text="Подробнее", callback_data=f'select_tovar {my_clients_list[11]}')))
                    await FSM_check_user.loading_ID.set()
    elif (message.text == "Главное меню"):
        Date_base.execute_query(f"""UPDATE business SET GOLD_BUSINESS = GOLD_BUSINESS-1 WHERE ID_BUSINESS={message.from_user.id}""")
        await state.finish()
        await message.answer('выберите действие', reply_markup=main_kb)





#SELECT DISTINCT `INN_CLIENT`, `NAME_CLIENT`,`SN_CLIENT`,`LN_CLIENT`,`ID_CLIENT` FROM `client`
#await bot.send_photo(callback.from_user.id, my_clients_list[8]





class FSM_add_credit(StatesGroup):
    ID_Client = State()
    All_tovars = State()
    ID_Tovar = State()
    Check = State()
    Instert = State()
    sebestoimost = State()
    price_to_pay = State()
    time_pay = State()
    INN_client = State()







@dp.callback_query_handler(text='dolg') # добавить долг
async def dolg_call(callback: types.CallbackQuery):
    my_clients = f""" SELECT * FROM client WHERE ID_BUSINESS = {callback.from_user.id}"""
    connection = Date_base.create_connection()
    with connection.cursor() as cursor:
        cursor.execute(my_clients)
        for my_clients_list in cursor.fetchall():
            await callback.bot.send_photo(callback.from_user.id, my_clients_list[8],
                                          f"Имя: {my_clients_list[2]}\nФамилия: {my_clients_list[3]}\nОтчество: {my_clients_list[4]}\nНомер телефона: {my_clients_list[5]} ")
            await callback.message.answer("^^^", reply_markup=InlineKeyboardMarkup().add(InlineKeyboardButton(text="Выбрать клиента", callback_data=f'select_client {my_clients_list[0]}')))

@dp.callback_query_handler(lambda x: x.data and x.data.startswith('select_client '))
async def callback_ran(callback: types.CallbackQuery, state: FSM_add_credit.ID_Client):
    async with state.proxy() as data:
        data['ID_Client'] = callback.data.replace('select_client ', "")
        #print(data['ID_Client'])
    await FSM_add_credit.All_tovars.set()
    await callback.message.answer("Перейти к выбору товара", reply_markup=ReplyKeyboardMarkup(resize_keyboard=True, one_time_keyboard=True).row(KeyboardButton('Перейти')))

@dp.message_handler(state=FSM_add_credit.All_tovars)
async def load_All_tovars(message: types.Message):
    all_tovar = f""" SELECT * FROM product WHERE ID_BUSINESSMAN = {message.from_user.id}"""
    connection = Date_base.create_connection()
    with connection.cursor() as cursor:
        cursor.execute(all_tovar)
        for all_tovar_list in cursor.fetchall():
            await bot.send_photo(message.from_user.id, all_tovar_list[9],
                                          f"Название продукта: {all_tovar_list[3]}\nКатегория товара: {all_tovar_list[2]}\nСебестоимость: {all_tovar_list[4]}\nЦена на продажу: {all_tovar_list[5]} ")
            await message.answer("^^^", reply_markup=InlineKeyboardMarkup().add(
                InlineKeyboardButton(text="Выбрать товар", callback_data=f'select_tovar {all_tovar_list[0]}')))
            await FSM_add_credit.ID_Tovar.set()



@dp.callback_query_handler(state=FSM_add_credit.ID_Tovar)
async def callback_ran_2(callback: types.CallbackQuery, state: FSMContext):
    async with state.proxy() as data:
        data['ID_Tovar'] = callback.data.replace('select_tovar ', "")
        #print(data['ID_Tovar'])
    await FSM_add_credit.Check.set()
    await callback.message.answer("Перейти к проверке",
                                  reply_markup=ReplyKeyboardMarkup(resize_keyboard=True,
                                                                   one_time_keyboard=True).row(
                                      KeyboardButton('Перейти')))


@dp.message_handler(state=FSM_add_credit.Check)
async def load_All_tovars(message: types.Message, state: FSMContext):
    async with state.proxy() as data:
        tovar_list = f""" SELECT * FROM product WHERE ID_PRODUCT = {data['ID_Tovar']}"""
        connection = Date_base.create_connection()
        with connection.cursor() as cursor:
            cursor.execute(tovar_list)
            for tovar_1 in cursor.fetchall():
                tovar = tovar_1[3]
                data['sebestoimost'] = tovar_1[4]
                data['price_to_pay'] = tovar_1[5]
                data['time_pay'] = tovar_1[7]
        client = f""" SELECT * FROM client WHERE ID_CLIENT  = {data['ID_Client']}"""
        connection = Date_base.create_connection()
        with connection.cursor() as cursor:
            cursor.execute(client)
            for client_1 in cursor.fetchall():
                client = client_1[2]
                data['INN_client'] = client_1[11]
        #print(f"Оформить товар '{tovar}' на {client}")
    await message.answer(f"Оформить товар '{tovar}' на {client}",reply_markup=client_accept)
    await FSM_add_credit.next()

@dp.message_handler(state=FSM_add_credit.Instert)
async def load_All_tovars(message: types.Message, state: FSMContext):
    if(message.text=="Подтвердить"):
        async with state.proxy() as data:
            Date_base.execute_query(f"INSERT INTO orders (ID_CLIENT, ID_BUSINESS, ID_PRODUCT,INN_CLIENT ,SEBESTOIMOST_PRODUCT, PRICE_PRODUCT, MONTH_OF_PRODUCT, DATE_ORDER, DATA_PLAS_MOUNTH, MONTH_OF_PRODUCT_LAST) VALUES ('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')"%(int(data['ID_Client']), int(message.from_user.id),int(data['ID_Tovar']),int(data['INN_client']),data['sebestoimost'], data['price_to_pay'],data['time_pay'] , datetime.today().strftime('%d.%m.%Y'),(datetime.now()+relativedelta(months=+1)).strftime('%d.%m.%Y'),data['time_pay'] ))
        await state.finish()
        await message.answer('Операция прошла успешно')
        await message.answer('выберите действие', reply_markup=main_kb)
    elif(message.text=="Отменить"):
        await state.finish()
        await message.answer('выберите действие', reply_markup=main_kb)


@dp.callback_query_handler(text='/my_clients') # мои клиенты
async def my_clients_call(callback: types.CallbackQuery):
    my_clients = f""" SELECT * FROM client WHERE ID_BUSINESS = {callback.from_user.id}"""
    connection = Date_base.create_connection()
    with connection.cursor() as cursor:
        cursor.execute(my_clients)
        for my_clients_list in cursor.fetchall():
            await callback.bot.send_photo(callback.from_user.id, my_clients_list[8],
                                          f"Имя: {my_clients_list[2]}\nФамилия: {my_clients_list[3]}\nОтчество: {my_clients_list[4]}\nНомер телефона Telegram: {my_clients_list[6]}\nНомер телефона WhatsApp: {my_clients_list[7]}")
    await callback.message.answer('выберите действие', reply_markup=main_kb)