from keyboards.admin import main_kb
from datetime import datetime, timedelta, date
from dateutil.relativedelta import relativedelta
import requests

from aiogram import types
from aiogram.dispatcher import FSMContext
from aiogram.dispatcher.filters.state import State, StatesGroup
from aiogram.types import InlineKeyboardMarkup, InlineKeyboardButton, ReplyKeyboardMarkup, KeyboardButton, \
    ReplyKeyboardRemove
from mysql.connector import Error
import random
import string

import Date_base
from create_bot import dp, bot
from names import WhatsApp, instance


class FSM_oplata_perMonth(StatesGroup):
    vibor_user = State()
    days = State()
    reason = State()
    accept_1 = State()
    accept_2 = State()
    accept_3 = State()

async  def cron():
    business = f""" SELECT * FROM orders WHERE DATA_PLAS_MOUNTH  = '{(datetime.now()).strftime('%d.%m.%Y')}'"""
    connection = Date_base.create_connection()
    with connection.cursor() as cursor:
        cursor.execute(business)
        for business_list in cursor.fetchall():
            client = f""" SELECT * FROM client WHERE ID_CLIENT  = '{business_list[1]}'"""
            with connection.cursor() as cursor:
                cursor.execute(client)
                for client_list in cursor.fetchall():
                    product = f""" SELECT * FROM product WHERE ID_PRODUCT  = '{business_list[3]}'"""
                    with connection.cursor() as cursor:
                        cursor.execute(product)
                        for product_list in cursor.fetchall():
                            if(business_list[6] > business_list[7]):
                                shop = f""" SELECT * FROM business WHERE ID_BUSINESS  = '{business_list[2]}'"""
                                with connection.cursor() as cursor:
                                    cursor.execute(shop)
                                    for shop_list in cursor.fetchall():
                                        await bot.send_message(business_list[2],f"Пользователь {client_list[2]} должен оплатить кредит за {product_list[3]} : {product_list[8]} coм\n Его номер : {client_list[5]} - позвонить", reply_markup=InlineKeyboardMarkup().add(InlineKeyboardButton(text="Подтвердить оплату", callback_data=f'oplatit {business_list[0]} {business_list[3]}'), InlineKeyboardButton(text="Дать отсрочку", callback_data=f'otsrochka {business_list[0]} {business_list[3]}')))
                                        url = f'https://api.chat-api.com/{instance}/sendMessage?token={WhatsApp}'
                                        if shop_list[10] == "":
                                            str_1 = ""
                                        else:
                                            str_1 = f"Можете оплатить на карту OptimaBank : {shop_list[10]}. "
                                        if shop_list[11] == "":
                                            str_2 = ""
                                        else:
                                            str_2 = f"Можете оплатить на кошелек paybox : https://api.paybox.money/payment.php?pg_order_id={shop_list[11]}&pg_merchant_id={business_list[2]}&pg_amount={product_list[8]}&pg_currency=KGS&pg_description={product_list[3]}&pg_salt={''.join(random.sample((string.ascii_letters + string.digits), 16))}&pg_sig={''.join(random.sample((string.ascii_letters + string.digits), 16))}. "
                                        if shop_list[14] == "":
                                            str_3 = ""
                                        else:
                                            str_3 = f"Можете оплатить на кошелек Элсом{shop_list[14]}. "
                                        if shop_list[15] == "":
                                            str_4 = ""
                                        else:
                                            str_4 = f"Можете оплатить на кошелек О!Деньги{shop_list[15]}. "
                                        if shop_list[16] == "":
                                            str_5 = ""
                                        else:
                                            str_5 = f"Можете оплатить на кошелек Balance{shop_list[16]}. "
                                        if shop_list[17] == "":
                                            str_6 = ""
                                        else:
                                            str_6 = f" Можете оплатить на кошелек MegaPay{shop_list[17]}."
                                        a = f"Уважаемый {client_list[2]} {client_list[3]}, вам нужно оплатить долг в магазин {shop_list[5]} за товар {product_list[3]} : {product_list[8]} coм за {business_list[8]} месяцев, у вас осталось еще {business_list[12]} месяц(-ев). {str_1+str_2+str_3+str_4+str_5+str_6}Телефон для связи {shop_list[3]}"
                                        data = ({"message" : a}, {"phone" : f"{client_list[7]}"})
                                        requests.post(url, json=data)
                                        print(requests.post(url, json=data))
                                        print(url, data)
                                        response = requests.get(url)
                                        print(response.json())


    return 0

@dp.callback_query_handler(lambda x: (x.data and x.data.startswith('oplatit ')) or (x.data and x.data.startswith('otsrochka '))) # добавить долг
async def dolg_call(callback: types.CallbackQuery, state: FSMContext):
    a = (callback.data).split(' ')
    print(a[2])
    async with state.proxy() as data:
        data['vibor_user'] = a[1]
    if(a[0] == 'otsrochka'):
        await callback.message.answer("Введите кол-во дней, на которое вы хотите дать отсрочку")
        await FSM_oplata_perMonth.days.set()
    elif(a[0] == 'oplatit'):
        product = f""" SELECT * FROM product WHERE ID_PRODUCT = '{int(a[2])}'"""
        connection = Date_base.create_connection()
        with connection.cursor() as cursor:
            cursor.execute(product)
            for product_list in cursor.fetchall():
                async with state.proxy() as data:
                    data['accept_1'] = int(product_list[8])
                    data['accept_2'] = int(a[1])
                    data['accept_3'] = int(a[2])
                await callback.message.answer(f"Сумма: {int(product_list[8])}\nДата оплаты: {datetime.now().strftime('%d.%m.%Y')}\n\n", reply_markup=InlineKeyboardMarkup().add(InlineKeyboardButton(text="Подтвердить", callback_data=f'accept')))


@dp.callback_query_handler(text = "accept")
async def accept_def(callback: types.CallbackQuery, state: FSMContext):
    async with state.proxy() as data:
        product = f""" SELECT * FROM product WHERE ID_PRODUCT = '{data['accept_3']}'"""
        connection = Date_base.create_connection()
        with connection.cursor() as cursor:
            cursor.execute(product)
            for product_list in cursor.fetchall():
                Date_base.execute_query(f"""UPDATE orders SET EVERYMONTH_PRICE = EVERYMONTH_PRICE + {int(product_list[8])} , DATA_PLAS_MOUNTH  = '{(datetime.now()+relativedelta(months=+1)).strftime('%d.%m.%Y')}' , MONTH_OF_PRODUCT_LAST = MONTH_OF_PRODUCT_LAST-1 WHERE ID_ORDER  = '{data['accept_2']}'""")
                await callback.message.answer('выберите действие', reply_markup=main_kb)

@dp.message_handler(state=FSM_oplata_perMonth.days)
async def load_input_serch_INN(message: types.Message, state: FSMContext):
    if((message.text).isdigit()==True and (int(message.text) > 0)):
        async with state.proxy() as data:
            data['days'] = message.text
        await message.answer("Введите причину отсрочки")
        await FSM_oplata_perMonth.reason.set()
    else:
        await message.answer("Введите кол-во дней, на которое вы хотите дать отсрочку")
        await FSM_oplata_perMonth.days.set()

@dp.message_handler(state=FSM_oplata_perMonth.reason)
async def load_input_serch_INN(message: types.Message, state: FSMContext):
    async with state.proxy() as data:
        data['reason'] = message.text
    async with state.proxy() as data:
        Date_base.execute_query(f"""UPDATE orders SET REASON_OTSROCHKA = '{str(data['reason'])}' , DATA_PLAS_MOUNTH  = '{str((datetime.now()+relativedelta(days=int(data['days']))).strftime('%d.%m.%Y'))}' WHERE ID_ORDER = '{int(data['vibor_user'])}'""")
    await state.finish()
    await message.answer('выберите действие', reply_markup=main_kb)
